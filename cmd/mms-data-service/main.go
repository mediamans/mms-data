// TODO: priority 1
package main

import (
	"flag"

	"bitbucket.org/mediamans/mms-data/pkg/db"
	_ "bitbucket.org/mediamans/mms-data/pkg/db/mongo"

	"bitbucket.org/mediamans/mms-data/pkg/service"

	"github.com/asim/go-micro/server"
	log "github.com/golang/glog"
)

func main() {
	defer log.Flush()

	// Initialise flags for service database
	db.InitFlags("service")

	// Initialise database connection
	db.Init()

	// Data service name
	server.Name = "mms.data.service"

	// Initialise Server
	server.Init()

	// Register Handlers
	// server.Register(
	// 	server.NewReceiver(
	// 		new(service.Data),
	// 	),
	// )

	// Initialize service
	service.Init()

	// Run server
	if err := server.Run(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	flag.Set("logtostderr", "true")
}
