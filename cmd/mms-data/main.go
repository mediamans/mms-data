// mms-data command line tool
// TODO: priority 3
package main

import (
	"fmt"
	"os"

	"bitbucket.org/mediamans/mms-data/pkg/cli"

	"github.com/gonuts/commander"
)

var app = &commander.Command{
	UsageLine: "mms-data",
	Short:     "command line tool for mms data",
}

func main() {
	err := app.Dispatch(os.Args[1:])
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}
}

func init() {
	app.Subcommands = []*commander.Command{
		cli.TestCommand,
	}
}
