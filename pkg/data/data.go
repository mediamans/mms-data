package data

import (
	"errors"
	"time"
)

// Driver - Database driver.
type Driver interface {
	InitFlags(string, FlagSet, interface{}) interface{}
	Dialer
}

// FlagSet -
type FlagSet interface {
	DurationVar(p *time.Duration, name string, value time.Duration, usage string)
	StringVar(p *string, name string, value string, usage string)
	BoolVar(p *bool, name string, value bool, usage string)
	Parse(arguments []string) error
	Parsed() bool
}

// Dialer - Dials database.
type Dialer interface {
	Dial(interface{}) (Database, error)
}

// Database - Database interface.
type Database interface {
	Namespace(string) Namespace // Gets or creates namespace
	Close() error
	Ping() error
}

// Namespace - Namespace interface.
type Namespace interface {
	Family(string) Family
}

// Family - Family interface.
type Family interface {
	Insert(interface{}, interface{}) error
	Query() Query

	SchemaLess() bool
	Schema() Schema
}

// Schema - Schema interface.
type Schema interface {
	Columns() ([]Column, error)
}

// ColumnType - ColumnType.
type ColumnType int

// Column - Column interface.
type Column interface {
	Name() string
	Type() ColumnType
	Index() *Index
	CreateIndex(*Index) error
}

// Query - Query interface.
type Query interface {
	Get(interface{}, interface{}) error
	Select(...string) Query
	Limit(int) Query
	Skip(int) Query
	Where(interface{}) Query
	All(interface{}) error
	One(interface{}) error
	Delete(...interface{}) error
	Upsert(interface{}) error
	Iter() Iter
}

// Iter - Query result iterator.
type Iter interface {
	Next(interface{}) bool
	Close() error
	Err() error
}

// Index - Index structure.
type Index struct {
	Key    []string
	Unique bool
	Expire time.Duration
}

// M - Data map shortcut.
type M map[string]interface{}

// ErrNotFound -
var ErrNotFound = errors.New("Not Found")

const (
	Bool ColumnType = iota
	Float
	Int
	Map
	Array
	String
	Struct
	Interface
)
