package service

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"bitbucket.org/mediamans/mms-data/pkg/db"
)

// DB - Service database.
var DB data.Database

// Init - Initializes service.
func Init() {
	DB = db.Get("service")
}
