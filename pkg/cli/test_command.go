package cli

import (
	"fmt"

	"github.com/gonuts/commander"
	"github.com/gonuts/flag"
)

const testCommandUsage = `
  runs cmd1 and exits.
  ex:
  $ my-cmd cmd1
  `

// TestCommand -
var TestCommand = &commander.Command{
	Run:       runTestCommand,
	UsageLine: "cmd1 [options]",
	Short:     "runs cmd1 and exits",
	Long:      testCommandUsage,
	Flag:      *flag.NewFlagSet("my-cmd-cmd1", flag.ExitOnError),
}

func init() {
	TestCommand.Flag.Bool("q", true, "only print error and warning messages, all other output will be suppressed")
}

func runTestCommand(cmd *commander.Command, args []string) error {
	name := "my-cmd-" + cmd.Name()
	quiet := cmd.Flag.Lookup("q").Value.Get().(bool)
	fmt.Printf("%s: hello from cmd1 (quiet=%v)\n", name, quiet)
	return nil
}
