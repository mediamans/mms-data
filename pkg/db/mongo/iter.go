package mongo

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"gopkg.in/mgo.v2"
)

// iter - MongoDB query iterator.
type iter struct {
	session *mgo.Session
	iter    *mgo.Iter
}

// Next -
func (i *iter) Next(result interface{}) bool {
	return i.iter.Next(result)
}

// Err -
func (i *iter) Err() error {
	err := i.iter.Err()
	if err == mgo.ErrNotFound {
		return data.ErrNotFound
	}
	return err
}

// Close - Closes iterator.
func (i *iter) Close() error {
	i.session.Close()
	return i.iter.Close()
}
