package mongo

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"gopkg.in/mgo.v2"
)

// namespace - MongoDB database.
type namespace struct {
	name string
	db   *database
}

// Family - MongoDB collection.
func (ns *namespace) Family(name string) data.Family {
	return &family{
		namespace: ns,
		name:      name,
	}
}

func (ns *namespace) clone() *mgo.Database {
	return ns.db.clone().DB(ns.name)
}
