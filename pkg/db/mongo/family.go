package mongo

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// family - MongoDB collection.
type family struct {
	namespace *namespace
	name      string
}

// Insert - Inserts document to collection under id.
func (f *family) Insert(id interface{}, entity interface{}) (err error) {
	col := f.clone()
	defer col.Database.Session.Close()

	if id == nil {
		return col.Insert(entity)
	}

	switch id.(type) {
	case string, bson.ObjectId:
		_, err = col.UpsertId(id, entity)
		return
	default:
		_, err = col.Upsert(id, entity)
	}

	return
}

// Query - Returns query constructor.
func (f *family) Query() data.Query {
	return &query{family: f}
}

// SchemaLess - Returns true - MongoDB is schema-less.
func (f *family) SchemaLess() bool {
	return true
}

// Schema - Returns MongoDB indexes.
func (f *family) Schema() data.Schema {
	return &schema{family: f}
}

func (f *family) clone() *mgo.Collection {
	return f.namespace.clone().C(f.name)
}
