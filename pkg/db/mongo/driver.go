package mongo

import (
	"fmt"

	"bitbucket.org/mediamans/mms-data/pkg/data"
	"bitbucket.org/mediamans/mms-data/pkg/db"
)

type driver struct {
	*dialer
}

func (d *driver) InitFlags(prefix string, flags data.FlagSet, o interface{}) interface{} {
	var opts *dialOptions
	switch o.(type) {
	case *dialOptions:
		opts = o.(*dialOptions)
	default:
		opts = new(dialOptions)
	}
	flags.StringVar(&opts.url, fmt.Sprintf("%s-url", prefix),
		"mongodb://127.0.0.1:27017", "MongoDB URL")
	return opts
}

func init() {
	db.RegisterDriver("mongo", &driver{new(dialer)})
}
