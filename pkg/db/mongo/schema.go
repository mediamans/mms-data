package mongo

import "bitbucket.org/mediamans/mms-data/pkg/data"

// schema - MongoDB collection indexes.
type schema struct {
	family *family
}

func (s *schema) Columns() ([]data.Column, error) {
	col := s.family.clone()
	defer col.Database.Session.Close()

	// Get indexes
	indexes, err := col.Indexes()
	if err != nil {
		return nil, err
	}

	// Create columns list
	list := []data.Column{}
	for _, index := range indexes {
		c := &column{
			name: index.Name,
			index: &data.Index{
				Key:    index.Key,
				Unique: index.Unique,
				Expire: index.ExpireAfter,
			},
		}
		list = append(list, c)
	}

	return list, nil
}
