package mongo

import (
	"time"

	"bitbucket.org/mediamans/mms-data/pkg/data"

	"gopkg.in/mgo.v2"
)

// database - MongoDB session.
type database struct {
	session *mgo.Session
}

// New - Creates new MongoDB session.
func New(url string) (data.Database, error) {
	var err error
	db := new(database)
	db.session, err = mgo.Dial(url)
	return db, err
}

// NewTimeout - Creates new MongoDB session with dial timeout.
func NewTimeout(url string, timeout time.Duration) (data.Database, error) {
	var err error
	db := new(database)
	db.session, err = mgo.DialWithTimeout(url, timeout)
	return db, err
}

// Namespace - Returns Mongo database by name.
func (db *database) Namespace(name string) data.Namespace {
	return &namespace{
		name: name,
		db:   db,
	}
}

// Ping - Pings session.
func (db *database) Ping() error {
	return db.session.Ping()
}

// Close - Closes session.
func (db *database) Close() error {
	if db.session != nil {
		db.session.Close()
	}
	return nil
}

// clone - Clones session.
func (db *database) clone() *mgo.Session {
	return db.session.Clone()
}
