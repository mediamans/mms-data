package mongo

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"gopkg.in/mgo.v2"
)

// query - MongoDB collection query.
type query struct {
	family *family

	fields map[string]int
	where  interface{}
	limit  int
	skip   int
}

// Get - Gets document by id.
func (q *query) Get(id interface{}, result interface{}) error {
	col := q.family.clone()
	defer col.Database.Session.Close()

	// Get by id
	err := col.FindId(id).One(result)
	if err == mgo.ErrNotFound {
		return data.ErrNotFound
	}

	return err
}

// Delete - Deletes all document matching query or by ID.
func (q *query) Delete(ids ...interface{}) error {
	col := q.family.clone()
	defer col.Database.Session.Close()

	// If no ids remove all matching query
	if len(ids) == 0 {
		_, err := col.RemoveAll(q.where)
		return err
	}

	// Remove all id's
	for _, id := range ids {
		err := col.RemoveId(id)
		if err == mgo.ErrNotFound {
			return data.ErrNotFound
		} else if err != nil {
			return err
		}
	}

	return nil
	// return col.FindId(id).One(result)
}

// All - Retrieves all documents matching query
func (q *query) All(result interface{}) error {
	col := q.family.clone()
	defer col.Database.Session.Close()
	return q.constructQuery(col).All(result)
}

// One - Retrieves one document matching query.
func (q *query) One(result interface{}) error {
	col := q.family.clone()
	defer col.Database.Session.Close()

	// Create new query
	query := col.Find(q.where)

	// Set fields to select if not all
	if len(q.fields) >= 1 {
		query.Select(q.fields)
	}

	err := query.One(result)
	if err == mgo.ErrNotFound {
		return data.ErrNotFound
	}
	return err
}

// Upsert -
func (q *query) Upsert(entity interface{}) error {
	col := q.family.clone()
	defer col.Database.Session.Close()

	// Get by id
	_, err := col.Upsert(q.where, entity)
	if err == mgo.ErrNotFound {
		return data.ErrNotFound
	}

	return err
}

// Limit -
func (q *query) Limit(n int) data.Query {
	q.limit = n
	return q
}

// Skip -
func (q *query) Skip(n int) data.Query {
	q.skip = n
	return q
}

// Where -
func (q *query) Where(where interface{}) data.Query {
	q.where = where
	return q
}

// Select -
func (q *query) Select(fields ...string) data.Query {
	f := make(map[string]int)
	for _, field := range fields {
		f[field] = 1
	}
	q.fields = f
	return q
}

// Iter -
func (q *query) Iter() data.Iter {
	col := q.family.clone()

	// Construct query
	query := q.constructQuery(col)

	// Create iterator
	return &iter{
		iter:    query.Iter(),
		session: col.Database.Session,
	}
}

func (q *query) constructQuery(col *mgo.Collection) *mgo.Query {
	// Create new query
	query := col.Find(q.where)

	// Set limit if not default
	if q.limit != 0 {
		query.Limit(q.limit)
	}

	// Set skip if not default
	if q.skip != 0 {
		query.Skip(q.skip)
	}

	// Set fields to select if not all
	if len(q.fields) >= 1 {
		query.Select(q.fields)
	}

	return query
}
