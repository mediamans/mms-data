package mongo

import (
	"fmt"

	"bitbucket.org/mediamans/mms-data/pkg/data"
)

type dialer struct {
}

type dialOptions struct {
	url string
}

func (d *dialer) Dial(opts interface{}) (data.Database, error) {
	switch opts.(type) {
	case string:
		return New(opts.(string))
	case *dialOptions:
		return d.dial(opts.(*dialOptions))
	default:
		return nil, fmt.Errorf("Cannot dial mongo with options %#v", opts)
	}
}

func (d *dialer) dial(opts *dialOptions) (data.Database, error) {
	return New(opts.url)
}
