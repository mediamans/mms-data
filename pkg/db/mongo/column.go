package mongo

import (
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"gopkg.in/mgo.v2"
)

// column - MongoDB collection column.
type column struct {
	family *family
	index  *data.Index
	name   string
}

// CreateIndex - Creates Index.
func (c *column) CreateIndex(index *data.Index) error {
	col := c.family.clone()
	defer col.Database.Session.Close()
	err := col.EnsureIndex(mgo.Index{
		Key:         index.Key,
		Unique:      index.Unique,
		ExpireAfter: index.Expire,
	})
	if err != nil {
		return err
	}
	c.index = index
	return nil
}

// data.Index - Returns column index.
func (c *column) Index() *data.Index {
	return c.index
}

// Type - Column type (anything).
func (c *column) Type() data.ColumnType {
	return data.Interface
}

// Name - Column type (from index name).
func (c *column) Name() string {
	return c.name
}
