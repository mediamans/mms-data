package mongo

import (
	"testing"

	"bitbucket.org/mediamans/mms-data/pkg/data"
)

func TestDatabase(t *testing.T) {
	var _ data.Database = &Database{}
}
