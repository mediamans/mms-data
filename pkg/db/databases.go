package db

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/mediamans/mms-data/pkg/data"
	log "github.com/golang/glog"
)

// Databases - Registry of drivers and databases.
type Databases interface {
	RegisterDriver(name string, driver data.Driver)
	InitFlags(string, string, data.FlagSet)
	Init(data.FlagSet)
	Get(string) data.Database
	GetDriver(string) data.Driver
}

type databases struct {
	databases map[string]*database
	drivers   map[string]data.Driver
}

type database struct {
	driverOpts map[string]interface{}
	driver     string
	db         data.Database
}

// Get - Gets database by name.
func (d *databases) Get(name string) data.Database {
	db := d.databases[name]
	if db == nil {
		return nil
	}
	return db.db
}

// GetDriver - Gets database driver by name.
func (d *databases) GetDriver(name string) data.Driver {
	return d.drivers[name]
}

// RegisterDriver - Registers database driver.
func (d *databases) RegisterDriver(name string, driver data.Driver) {
	log.V(6).Infof("Registered driver %s", name)
	d.drivers[name] = driver
}

// Init - Initializes database from flags.
func (d *databases) Init(flags data.FlagSet) {
	if flags == nil {
		flags = flag.CommandLine
	}

	if !flags.Parsed() {
		flags.Parse(os.Args[1:])
	}

	for name, db := range d.databases {
		if db.driver == "" {
			log.Infof("Database %s has no driver", name)
			continue
		}
		err := d.InitDB(name, db.driver, db.driverOpts[db.driver])
		if err != nil {
			log.Fatal(err)
		}
	}
}

// InitDB - Initializes database.
func (d *databases) InitDB(name, driver string, options interface{}) error {
	log.Infof("Initializing db %s driver:%s", name, driver)

	// Get driver
	drv := d.GetDriver(driver)
	if drv == nil {
		return fmt.Errorf("Database driver %s was not found", driver)
	}

	// Get database or create
	db := d.databases[name]
	if db == nil {
		db = new(database)
		db.driverOpts = make(map[string]interface{})
		db.driverOpts[driver] = options
	}

	// Connect database
	var err error
	d.databases[name].db, err = drv.Dial(options)
	if err != nil {
		return fmt.Errorf("Database %s driver %s dial error: %v", name, db.driver, err)
	}

	d.databases[name] = db
	return nil
}

// InitFlags - Initializes flags for database.
func (d *databases) InitFlags(name, def string, flags data.FlagSet) {
	if flags == nil {
		flags = flag.CommandLine
	}

	// Flags prefix
	prefix := fmt.Sprintf("data-%s", name)

	// Add database driver flag
	db := d.databases[name]
	if db == nil {
		db = &database{driverOpts: make(map[string]interface{})}
	}
	flags.StringVar(&db.driver, fmt.Sprintf("%s-driver", prefix), def,
		fmt.Sprintf("%s database driver", name))

	// Add all drivers flags
	for drvname, driver := range d.drivers {
		opts := driver.InitFlags(fmt.Sprintf("%s-%s", prefix, drvname), flags,
			db.driverOpts[drvname])
		db.driverOpts[drvname] = opts
	}

	d.databases[name] = db
}
