package db

import (
	"flag"

	"bitbucket.org/mediamans/mms-data/pkg/data"
)

// DefaultDatabases - Default registry of dialers and databases.
var DefaultDatabases Databases = &databases{
	databases: make(map[string]*database),
	drivers:   make(map[string]data.Driver),
}

// Init - Initializes database from flags.
func Init() {
	DefaultDatabases.Init(flag.CommandLine)
}

// Get - Gets database by name.
func Get(name string) data.Database {
	return DefaultDatabases.Get(name)
}

// GetDriver - Gets database driver by name.
func GetDriver(name string) {
	DefaultDatabases.GetDriver(name)
}

// InitFlags - Initializes flags for database.
func InitFlags(name string) {
	DefaultDatabases.InitFlags(name, "", flag.CommandLine)
}

// InitFlagsDefault - Initializes flags for database.
func InitFlagsDefault(name, def string) {
	DefaultDatabases.InitFlags(name, def, flag.CommandLine)
}

// RegisterDriver - Registers database driver.
func RegisterDriver(name string, driver data.Driver) {
	DefaultDatabases.RegisterDriver(name, driver)
}
