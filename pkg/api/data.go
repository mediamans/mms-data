package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"

	"bitbucket.org/mediamans/mms-data/pkg/api/util"
	"bitbucket.org/mediamans/mms-data/pkg/data"
	"bitbucket.org/mediamans/mms-data/pkg/db"

	"github.com/emicklei/go-restful"

	log "github.com/golang/glog"
)

// Options - Data resource options.
type Options struct {
	Prefix    string
	Data      string
	Namespace string
	Family    string
	Entity    interface{}
	List      interface{}
}

// DataResource - Data REST resource.
type DataResource struct {
	entityType reflect.Type
	listType   reflect.Type
	data       data.Family
}

// Init - Initializes data RESTful webservice.
func Init(opts *Options) (ws *restful.WebService) {
	ws = new(restful.WebService)
	ws.
		ApiVersion("v1").
		Path(opts.Prefix).
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON).
		Doc(opts.Family)

	// Set no prefix now
	opts.Prefix = ""
	AddToWebservice(opts, ws)
	return
}

// AddToWebservice - Adds data api to webservice.
func AddToWebservice(opts *Options, ws *restful.WebService) {
	// Get database family
	family := db.Get(opts.Data).Namespace(opts.Namespace).Family(opts.Family)

	// New api resource
	r := NewResource(family, opts.Entity)

	ws.Route(
		ws.GET(fmt.Sprintf("%s/", opts.Prefix)).
			To(r.Query).
			Doc(fmt.Sprintf("find all %s", opts.Family)).
			Writes(opts.List).
			Param(ws.QueryParameter("query", "Query (json)").DataType("string")).
			Operation("Query"),
	)

	ws.Route(
		ws.GET(fmt.Sprintf("%s/{id}", opts.Prefix)).
			To(r.FindByID).
			Doc(fmt.Sprintf("find %s by id", opts.Family)).
			Param(ws.PathParameter("id", "Resource ID").DataType("string")).
			Writes(opts.Entity).
			Operation("FindByID"),
	)

	ws.Route(
		ws.POST(opts.Prefix).
			To(r.Insert).
			Doc(fmt.Sprintf("insert %s", opts.Family)).
			Reads(opts.Entity).
			Operation("Insert"),
	)

	ws.Route(
		ws.POST(fmt.Sprintf("%s/{id}", opts.Prefix)).
			To(r.InsertUnderID).
			Doc(fmt.Sprintf("insert %s", opts.Family)).
			Param(ws.PathParameter("id", "Resource ID").DataType("string")).
			Reads(opts.Entity).
			Operation("InsertUnderID"),
	)

	ws.Route(
		ws.PUT(fmt.Sprintf("%s/{id}", opts.Prefix)).
			To(r.InsertUnderID).
			Doc(fmt.Sprintf("update %s", opts.Family)).
			Param(ws.PathParameter("id", "Resource ID").DataType("string")).
			Reads(opts.Entity).
			Operation("Update"),
	)

	ws.Route(
		ws.DELETE(fmt.Sprintf("%s/{id}", opts.Prefix)).
			To(r.DeleteByID).
			Doc(fmt.Sprintf("delete %s by id", opts.Family)).
			Param(ws.PathParameter("id", "Resource ID").DataType("string")).
			Writes(opts.Entity).
			Operation("DeleteByID"),
	)
}

// NewResource -
func NewResource(family data.Family, entity interface{}) *DataResource {
	entityType := reflect.TypeOf(entity)
	return &DataResource{
		entityType: entityType,
		listType:   reflect.SliceOf(entityType),
		data:       family,
	}
}

// Query - makes query on data family.
func (r *DataResource) Query(request *restful.Request, response *restful.Response) {
	// Create query
	query := r.data.Query()

	// Get where clause from query parameter
	q := request.QueryParameter("query")
	if q != "" {
		var where data.M
		if err := json.Unmarshal([]byte(q), &where); err != nil {
			util.WriteError(response, http.StatusBadRequest, fmt.Sprintf("%v in query parameter", err))
			return
		}
		query.Where(where)
	}

	// Create new slice
	v := reflect.New(r.listType)

	// Get resources
	if err := query.All(v.Interface()); err != nil {
		util.WriteDataError(response, err)
		return
	}

	// Write resources
	if err := response.WriteAsJson(v.Interface()); err != nil {
		log.Warning(err)
	}
}

// FindByID - finds resource by id.
func (r *DataResource) FindByID(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("id")

	// Create new value
	v := reflect.New(r.entityType)

	// Get resource
	err := r.data.Query().Get(id, v.Interface())
	if err != nil {
		util.WriteDataError(response, err)
		return
	}

	// Write resource
	if err := response.WriteAsJson(v.Interface()); err != nil {
		log.Warning(err)
	}
}

// DeleteByID - finds resource by id.
func (r *DataResource) DeleteByID(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("id")

	// Delete resource
	err := r.data.Query().Delete(id)
	if err != nil {
		util.WriteDataError(response, err)
		return
	}

	// Write response ok-no-content
	response.WriteHeader(http.StatusNoContent)
}

// Insert - inserts resource.
func (r *DataResource) Insert(request *restful.Request, response *restful.Response) {
	// Get resource
	v := reflect.New(r.entityType)
	if err := request.ReadEntity(v.Interface()); err != nil {
		util.WriteError(response, http.StatusBadRequest, err.Error())
		return
	}

	// Save resource
	if err := r.data.Insert(nil, v.Interface()); err != nil {
		util.WriteDataError(response, err)
		return
	}

	// Write resource
	if err := response.WriteAsJson(v.Interface()); err != nil {
		log.Warning(err)
	}
}

// InsertUnderID - inserts resource under id.
func (r *DataResource) InsertUnderID(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("id")

	// Get resource
	v := reflect.New(r.entityType)
	if err := request.ReadEntity(v.Interface()); err != nil {
		util.WriteError(response, http.StatusBadRequest, err.Error())
		return
	}

	// Save resource
	if err := r.data.Insert(id, v.Interface()); err != nil {
		util.WriteDataError(response, err)
		return
	}

	// Write resource
	if err := response.WriteAsJson(v.Interface()); err != nil {
		log.Warning(err)
	}
}
