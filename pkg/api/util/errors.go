package util

import (
	"net/http"
	"strings"

	"bitbucket.org/mediamans/mms-data/pkg/data"
	"github.com/emicklei/go-restful"

	log "github.com/golang/glog"
)

// WriteError -
func WriteError(response *restful.Response, code int, msg string) {
	// Get status text
	name := http.StatusText(code)

	// Remove status text prefix from message
	if strings.HasPrefix(msg, name) {
		msg = strings.Trim(msg[len(name):], ": ")
	}

	// Write response status code
	response.WriteHeader(code)

	// Write JSON error
	response.WriteAsJson(struct {
		State   string `json:"state"`
		Message string `json:"error_message"`
		Name    string `json:"error_name"`
	}{
		State:   "error",
		Name:    name,
		Message: msg,
	})
}

// WriteDataError -
func WriteDataError(response *restful.Response, err error) {
	if err == data.ErrNotFound {
		WriteError(response, http.StatusNotFound, "resource not found")
		return
	} else if err != nil {
		WriteError(response, http.StatusInternalServerError, err.Error())
		log.Warning(err)
	}
}
